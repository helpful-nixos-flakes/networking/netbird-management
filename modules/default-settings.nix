{  
  Stuns = [
    {
      Proto = "udp";
      URI = null;
      Username = "";
      Password = null;
    }
  ];

  TURNConfig = {
    Turns = [
      {
        Proto = "udp";
        URI = null;
        Username = "netbird";
        Password = "netbird";
      }
    ];

    CredentialsTTL = "12h";
    Secret = "not-secure-secret";
    TimeBasedCredentials = false;
  };

  Signal = {
    Proto = "https";
    URI = null;
    Username = "";
    Password = null;
  };

  ReverseProxy = {
    TrustedHTTPProxies = [ ];
    TrustedHTTPProxiesCount = 0;
    TrustedPeers = [ "0.0.0.0/0" ];
  };

  Datadir = null;
  DataStoreEncryptionKey = "very-insecure-key";

  HttpConfig = {
    Address = null;
    OIDCConfigEndpoint = null;
    IdpSignKeyRefreshEnabled = true;
  };

  StoreConfig = {
    Engine = "sqlite";
  };

  IdpManagerConfig = {
    ManagerType = "none";
    ClientConfig = {
      Issuer = "";
      TokenEndpoint = "";
      ClientID = "netbird";
      ClientSecret = "";
      GrantType = "client_credentials";
    };

    ExtraConfig = { };
    Auth0ClientCredentials = null;
    AzureClientCredentials = null;
    KeycloakClientCredentials = null;
    ZitadelClientCredentials = null;
  };

  DeviceAuthorizationFlow = {
    Provider = "none";
    ProviderConfig = {
      Audience = "netbird";
      Domain = null;
      ClientID = "netbird";
      TokenEndpoint = null;
      DeviceAuthEndpoint = "";
      Scope = "openid profile email";
      UseIDToken = false;
    };
  };

  PKCEAuthorizationFlow = {
    ProviderConfig = {
      Audience = "netbird";
      ClientID = "netbird";
      ClientSecret = "";
      AuthorizationEndpoint = "";
      TokenEndpoint = "";
      Scope = "openid profile email";
      RedirectURLs = [ "http://localhost:53000" ];
      UseIDToken = false;
    };
  };
}