{ config, lib, pkgs, netbird-coturn, utils, ... }:

let
  inherit (lib) optional mkEnableOption mkIf mkOption mkPackageOption recursiveUpdate types;
  inherit (utils) escapeSystemdExecArgs genJqSecretsReplacementSnippet;
  
  stateDir = "/var/lib/netbird-management";

  moduleSettings = {
    DataStoreEncryptionKey._secret = cfg.dataStoreEncryptionKeyFile;
    HttpConfig = {
      Address = "127.0.0.1:${builtins.toString cfg.port}";
      AuthAudience = cfg.authAudience;
      AuthIssuer = cfg.authIssuer;
      OIDCConfigEndpoint = if cfg.oidcConfigEndpoint == null then
          "${cfg.authIssuer}/.well-known/openid-configuration"
        else
          cfg.oidcConfigEndpoint;
    };
    IdpManagerConfig.ClientConfig = { 
      Issuer = cfg.authIssuer;
      ClientSecret._secret = cfg.clientSecretFile;
    };
    PKCEAuthorizationFlow.ProviderConfig = {
      Audience = cfg.authAudience;
    };
    Signal = {
      URI = "${cfg.domain}:${builtins.toString cfg.signalPort}";
    };
    Stuns = let stunDomain = if cfg.stunDomain == null then cfg.turnDomain else cfg.stunDomain; in [
      # need the whole things since it's a list
      {
        Proto = "udp";
        URI = "stun:${stunDomain}:${builtins.toString cfg.stunPort}";
        Username = "";
        Password = null;
      }
    ];
    TURNConfig = {
      Secret._secret = cfg.turnSecretFile;
      Turns = [
        # need the whole things since it's a list
        {
          Proto = "udp";
          URI = "turn:${cfg.turnDomain}:${builtins.toString cfg.turnPort}";
          Username = cfg.turnUsername;
          Password._secret = cfg.turnPasswordFile;
        }
      ];
    };
  };
  
  # update the management config.json file recursively a couple of times
  settingsFormat = pkgs.formats.json { };
  defaultSettings = import ./default-settings.nix;
  moduleConfig = recursiveUpdate defaultSettings moduleSettings;
  managementConfig = recursiveUpdate moduleConfig cfg.settings;
  managementFile = settingsFormat.generate "config.json" managementConfig;
  
  cfg = config.services.netbird-management;
in {
  options.services.netbird-management = {
    enable = mkEnableOption "Netbird management server";
    package = mkPackageOption pkgs "netbird" { };

    logLevel = mkOption {
      type = types.enum [ "debug" "info" "warn" "error" ];
      default = "info";
      description = "Sets the Netbird log level.";
    };

    port = mkOption {
      type = types.port;
      default = 8011;
      description = "Internal port of the management server.";
    };

    signalPort = mkOption {
      type = types.port;
      default = 8012;
      description = "Netbird signal port.";
    };

    domain = mkOption {
      type = types.str;
      description = "The domain under which the management API runs.";
    };

    dnsDomain = mkOption {
      type = types.str;
      default = "netbird.selfhosted";
      description = "Domain used for peer resolution.";
    };

    stunDomain = mkOption {
      type = types.nullOr types.str;
      default = null;
      example = "stun.mydomain.tld";
      description = "The domain of the STUN server to use.";
    };

    stunPort = mkOption {
      type = types.port;
      default = 3478;
      description = "The port of the STUN server to use.";
    };

    turnDomain = mkOption {
      type = types.str;
      example = "turn.mydomain.tld";
      description = "The domain of the TURN server to use.";
    };

    turnPort = mkOption {
      type = types.port;
      default = 3478;
      description = "The port of the TURN server to use.";
    };

    turnUsername = mkOption {
      type = types.str;
      default = "netbird";
      description = "The username for the TURN server.";
    };

     turnPasswordFile = mkOption {
      type = types.path;
      example = /var/lib/netbird-coturn/password;
      description = "The path to a file containing the password for the TURN server.";
    };

    turnSecretFile = mkOption {
      type = types.path;
      example = /var/lib/netbird-coturn/secret;
      description = "The path to a file containing the TURN server's secret.";
    };

    dataStoreEncryptionKeyFile = mkOption {
      type = types.path;
      example = /var/lib/netbird-managmement/dspasswd;
      description = "The path to a file containing the data store encryption key.";
    };
    
    clientSecretFile = mkOption {
      type = types.path;
      example = /var/lib/netbird-managmement/client-secret;
      description = "The path to a file containing the IDP client secret for the managing user.";
    };

    singleAccountModeDomain = mkOption {
      type = types.str;
      default = "netbird.selfhosted";
      description = ''
        Enables single account mode.
        This means that all the users will be under the same account grouped by the specified domain.
        If the installation has more than one account, the property is ineffective.
      '';
    };

    disableAnonymousMetrics = mkOption {
      type = types.bool;
      default = true;
      description = "Disables push of anonymous usage metrics to NetBird.";
    };

    disableSingleAccountMode = mkOption {
      type = types.bool;
      default = false;
      description = ''
        If set to true, disables single account mode.
        The `singleAccountModeDomain` property will be ignored and every new user will have a separate NetBird account.
      '';
    };

    oidcConfigEndpoint = mkOption {
      type = types.nullOr types.str;
      description = "The oidc discovery endpoint.";
      default = null;
      example = "https://my.auth.tld:1234/.well-known/openid-configuration";
    };

    authIssuer = mkOption {
      type = types.str;
      example = "https://my.auth.tld:1234";
      description = "The auth issuer for the Netbird application.";
    };

    authAudience = mkOption {
      type = types.str;
      example = "netbird-client";
      description = "The auth audience for the Netbird application.";
    };

    settings = mkOption {
      inherit (settingsFormat) type;
      default = { };
      example = { DataStoreEncryptionKey._secret = "/var/lib/my/secret"; };
      description = "Netbird management server configuration.  This is a direct translation to the management configuration file.";
    };
  };

  config = mkIf cfg.enable {
    systemd.services.netbird-management = {
      description = "The management server for Netbird, a wireguard VPN";
      documentation = [ "https://netbird.io/docs/" ];

      after = [ "network.target" ];
      wantedBy = [ "multi-user.target" ];
      restartTriggers = [ managementFile ];

      preStart = genJqSecretsReplacementSnippet managementConfig "${stateDir}/management.json";

      serviceConfig = {
        ExecStart = escapeSystemdExecArgs ([
          "${cfg.package}/bin/netbird-mgmt"
          "management"
          "--config"
          "${stateDir}/management.json"
          "--port"
          cfg.port
          "--datadir"
          "${stateDir}/data"
          "--log-file"
          "console"
          "--log-level"
          cfg.logLevel
          "--dns-domain"
          cfg.dnsDomain
          "--single-account-mode-domain"
          cfg.singleAccountModeDomain
        ] ++ (optional cfg.disableAnonymousMetrics "--disable-anonymous-metrics")
          ++ (optional cfg.disableSingleAccountMode "--disable-single-account-mode"));

        Restart = "always";
        RuntimeDirectory = "netbird-management";
        StateDirectory = [
          "netbird-management"
          "netbird-management/data"
        ];
        WorkingDirectory = stateDir;

        # hardening
        LockPersonality = true;
        MemoryDenyWriteExecute = true;
        NoNewPrivileges = true;
        PrivateMounts = true;
        PrivateTmp = true;
        ProtectClock = true;
        ProtectControlGroups = true;
        ProtectHome = true;
        ProtectHostname = true;
        ProtectKernelLogs = true;
        ProtectKernelModules = true;
        ProtectKernelTunables = true;
        ProtectSystem = true;
        RemoveIPC = true;
        RestrictNamespaces = true;
        RestrictRealtime = true;
        RestrictSUIDSGID = true;
      };

      stopIfChanged = false;
    };
  };
}