# Netbird Managment Server Nixos Flake
This flake runs the Netbird managment server on localhost.  It also includes an `enableNginx` option for easy reverse-proxy configuration.

## Basic Configuration

### Flake import:
```nix
{
  description = "Netbird Managment Host Flake";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.05";
    netbird-managment.url = "git+https://gitlab.com/helpful-nixos-flakes/networking/netbird-managment";
    netbird-managment.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, ... }@attrs: {
    nixosConfigurations.myhost = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      specialArgs = attrs;
      modules = [ ./configuration.nix ];
    };
  };
}

```

### `configuration.nix`:
```nix
{ config, lib, pkgs, netbird-managment, ... }:

{
  imports = [ netbird-management.nixosModules.x86_64-linux.default ];

  # Minimum configuration that will most likely require more depending on IDP:
  config.services.netbird-managment = {
    enable = true;
    domain = "netbird.mydomain.tld";
    dataStoreEncryptionKeyFile = "/var/lib/netbird-management/ds-key";
    clientSecretFile = "/var/lib/netbird-management/client-secret";
    authIssuer = "https://auth0.mydomain.com:1234";
    turnDomain = "turn.mydomain.tld";
    turnPasswordFile = "/var/lib/coturn/password";
    turnSecretFile = "/var/lib/coturn/secret";
  };
}

```